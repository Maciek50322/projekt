﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Zoo.Data.Sql.DAO;

namespace Zoo.Data.Sql.DAOConfigurations
{
    public class ZwierzeConfiguration : IEntityTypeConfiguration<Zwierze>
    {
        public void Configure(EntityTypeBuilder<Zwierze> builder)
        {
            builder.Property(x => x.ZwierzeId).IsRequired();
            builder.Property(x => x.KlatkaId).IsRequired();
            builder.Property(x => x.Typ).IsRequired();
            
            builder.HasOne(x => x.Klatka)
                .WithMany(x => x.Zwierze)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.KlatkaId);
            builder.HasOne(x => x.Opiekun)
                .WithMany(x => x.Zwierze)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.OpiekunId);
            builder.ToTable("Zwierze");
        }
    }
}