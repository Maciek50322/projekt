﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Zoo.Data.Sql.DAO;

namespace Zoo.Data.Sql.DAOConfigurations
{
    public class PracownikCofiguration : IEntityTypeConfiguration<Pracownik>
    {
        public void Configure(EntityTypeBuilder<Pracownik> builder)
        {
            builder.Property(x => x.PracownikId).IsRequired();
            builder.Property(x => x.Imie).IsRequired();
            builder.Property(x => x.Nazwisko).IsRequired();
            builder.Property(x => x.Placa).IsRequired();
            builder.Property(x => x.StanowiskoId).IsRequired();

            builder.HasOne(x => x.Stanowisko)
                .WithMany(x => x.Pracownik)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.StanowiskoId);
            builder.ToTable("Pracownik");
        }
    }
}