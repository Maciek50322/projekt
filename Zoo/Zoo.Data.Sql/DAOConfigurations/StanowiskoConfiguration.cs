﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Zoo.Data.Sql.DAO;

namespace Zoo.Data.Sql.DAOConfigurations
{
    public class StanowiskoConfiguration:IEntityTypeConfiguration<Stanowisko>
    {
        public void Configure(EntityTypeBuilder<Stanowisko> builder)
        {
            builder.Property(x => x.StanowiskoId).IsRequired();
            builder.Property(x => x.Nazwa).IsRequired();

            builder.ToTable("Stanowisko");
        }
    }
}