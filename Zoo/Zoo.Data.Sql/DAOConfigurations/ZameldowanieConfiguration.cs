﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Zoo.Data.Sql.DAO;

namespace Zoo.Data.Sql.DAOConfigurations
{
    public class ZameldowanieConfiguration:IEntityTypeConfiguration<Zameldowanie>
    {
        public void Configure(EntityTypeBuilder<Zameldowanie> builder)
        {
            builder.Property(x => x.ZwierzeId).IsRequired();
            builder.Property(x => x.PracownikId).IsRequired();
            
            builder.HasOne(x => x.Pracownik)
                .WithMany(x => x.Zameldowanie)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.PracownikId);
            builder.HasOne(x => x.Zwierze)
                .WithMany(x => x.Zameldowanie)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.ZwierzeId);
            builder.ToTable("Zameldowanie");
        }
    }
}