﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Zoo.Data.Sql.DAO;

namespace Zoo.Data.Sql.DAOConfigurations
{
    public class KlatkaConfiguration: IEntityTypeConfiguration<Klatka>
    {
        public void Configure(EntityTypeBuilder<Klatka> builder)
        {
            builder.Property(k => k.KlatkaId).IsRequired();
            builder.Property(k => k.Typ).IsRequired();
            builder.ToTable("Klatka");
        }
    }
}