

using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Crypto.Tls;
using Zoo.Data.Sql.DAO;
using Zoo.Data.Sql.DAOConfigurations;

namespace Zoo.Data.Sql
{

    public class ZooDbContext : DbContext
    {
        public ZooDbContext(DbContextOptions<ZooDbContext> options) : base(options) { }
        public virtual DbSet<Klatka> Klatka { get; set; }
        public virtual DbSet<Pracownik> Pracownik { get; set; }
        public virtual DbSet<Stanowisko> Stanowisko { get; set; }
        public virtual DbSet<Zameldowanie> Zameldowanie { get; set; }
        public virtual DbSet<Zwierze> Zwierze { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            //base.OnModelCreating(builder);
            builder.ApplyConfiguration(new KlatkaConfiguration());
            builder.ApplyConfiguration(new PracownikCofiguration());
            builder.ApplyConfiguration(new StanowiskoConfiguration());
            builder.ApplyConfiguration(new ZameldowanieConfiguration());
            builder.ApplyConfiguration(new ZwierzeConfiguration());
        }
    }
}