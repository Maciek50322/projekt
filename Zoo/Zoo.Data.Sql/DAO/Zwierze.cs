﻿using System.Collections.Generic;

namespace Zoo.Data.Sql.DAO
{
    public class Zwierze
    {
        public Zwierze()
        {
            Zameldowanie = new List<Zameldowanie>();
        }

        public int ZwierzeId { get; set; }
        public string Nazwa { get; set; }
        public string Typ { get; set; }
        public int KlatkaId { get; set; }
        public int OpiekunId { get; set; }

        public virtual Pracownik Opiekun { get; set; }
        public virtual Klatka Klatka { get; set; }
    
        public virtual ICollection<Zameldowanie> Zameldowanie { get; set; }
    }
}