﻿using System.Collections.Generic;

namespace Zoo.Data.Sql.DAO
{
    public class Stanowisko
    {
        public Stanowisko()
        {
            Pracownik = new List<Pracownik>();
        }

        public int StanowiskoId { get; set; }
        public string Nazwa { get; set; }

        public virtual ICollection<Pracownik> Pracownik { get; set; }

    }
}