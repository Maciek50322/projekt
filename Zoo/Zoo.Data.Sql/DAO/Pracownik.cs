﻿using System.Collections.Generic;

namespace Zoo.Data.Sql.DAO
{
    public class Pracownik
    {
        public Pracownik ()
        {
            Zameldowanie = new List<Zameldowanie>();
            Zwierze = new List<Zwierze>();
        }
    
        public int PracownikId {get; set;}
        public string Imie {get; set;}
        public string Nazwisko {get; set;}
        public double Placa {get; set;}
        public int StanowiskoId { get; set; }

        public virtual Stanowisko Stanowisko { get; set; }
        
        public virtual ICollection<Zameldowanie> Zameldowanie { get; set; }
        public virtual ICollection<Zwierze> Zwierze { get; set; }
    }
}