﻿using System.Collections.Generic;

namespace Zoo.Data.Sql.DAO
{
    public class Klatka
    {
        public Klatka()
        {
            Zwierze = new List<Zwierze>();
        }

        public int KlatkaId { get; set; }
        public string Karmienie { get; set; }
        public string Typ { get; set; }
        

        public virtual ICollection<Zwierze> Zwierze { get; set; }
    }
}