﻿using System;
using System.Collections.Generic;

namespace Zoo.Data.Sql.DAO
{
    public class Zameldowanie
    {
        public Zameldowanie()
        {
            
        }

        public int ZameldowanieId { get; set; }
        public DateTime Tzameldowania { get; set; }
        public DateTime Twymeldowanie { get; set; }
        public int PracownikId { get; set; }
        public int ZwierzeId { get; set; }

        public virtual Pracownik Pracownik { get; set; }
        public virtual Zwierze Zwierze { get; set; }
    }
}