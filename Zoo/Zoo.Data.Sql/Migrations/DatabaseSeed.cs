﻿using System;
using System.Collections;
using System.Collections.Generic;
using Zoo.Data.Sql.DAO;

namespace Zoo.Data.Sql.Migrations
{
    public class DatabaseSeed
    {
        private readonly ZooDbContext _context;

        public DatabaseSeed(ZooDbContext context)
        {
            _context = context;
        }

        public void Seed()
        {
            var stanowiskoList = BuildStanowiskoList();
            _context.Stanowisko.AddRange(stanowiskoList);
            _context.SaveChanges();
            
            var pracownikList = BuildPracownikList();
            _context.Pracownik.AddRange(pracownikList);
            _context.SaveChanges();
            
            var klatkaList = BuildKlatkaList();
            _context.Klatka.AddRange(klatkaList);
            _context.SaveChanges();
            
            var zwierzeList = BuildZwierzeList();
            _context.Zwierze.AddRange(zwierzeList);
            _context.SaveChanges();
            
            var zameldowanieList = BuildZameldowanieList(pracownikList);
            _context.Zameldowanie.AddRange(zameldowanieList);
            _context.SaveChanges();
        }

        private IEnumerable<Klatka> BuildKlatkaList()
        {
            var klatkaList = new List<Klatka>();

            for (int i = 1; i <= 2; i++)
            {
                var klatka = new Klatka()
                {
                    KlatkaId = i,
                    Karmienie = i + "x dziennie",
                    Typ = "Dla " + (10-i) + " @"
                };
                klatkaList.Add(klatka);
            }
            return klatkaList;
        }
        
        private IEnumerable<Pracownik> BuildPracownikList()
        {
            var pracownikList = new List<Pracownik>();
            
            for (int i = 1; i <= 5; i++)
            {
                var pracownik = new Pracownik()
                {
                    PracownikId = i,
                    Imie="Steve" + i,
                    Nazwisko="Nonym" + i,
                    StanowiskoId = (i%2) + 1,
                    Placa = i*543
                };
                pracownikList.Add(pracownik);
            }
            return pracownikList;
        }
        
        private IEnumerable<Stanowisko> BuildStanowiskoList()
        {
            var stanowiskoList = new List<Stanowisko>();

            for (int i = 1; i <= 2; i++)
            {
                stanowiskoList.Add(new Stanowisko()
                {
                    StanowiskoId = i,
                    Nazwa = "Stanowisko " + i
                });
            }
            return stanowiskoList;
        }
        
        private IEnumerable<Zameldowanie> BuildZameldowanieList(
            IEnumerable<Pracownik> pracownikList)
        {
            var zameldowanieList = new List<Zameldowanie>();

            int i = 0;
            foreach (var pracownik in pracownikList)
            {
                for (int j = 1; j < 4; j++)
                {
                    zameldowanieList.Add(new Zameldowanie()
                    {
                        ZameldowanieId = i*3 + j,
                        PracownikId = pracownik.PracownikId,
                        ZwierzeId = i + 1,
                        Tzameldowania = DateTime.Today,
                        Twymeldowanie = DateTime.Now
                    });
                }

                i++;
            }
            return zameldowanieList;
        }
        
        private IEnumerable<Zwierze> BuildZwierzeList()
        {
            var zwierzeList = new List<Zwierze>();

            for (int i = 1; i <= 5; i++)
            {
                zwierzeList.Add(new Zwierze() 
                {
                    ZwierzeId = i,
                    Nazwa = "@ " + i,
                    Typ = "Drzewna",
                    OpiekunId = i,
                    KlatkaId = ((i%2) + 1)
                });
            }
            return zwierzeList;
        }
        
    }
}